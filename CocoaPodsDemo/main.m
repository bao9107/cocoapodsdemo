//
//  main.m
//  CocoaPodsDemo
//
//  Created by coco on 14-9-29.
//  Copyright (c) 2014年 zbl. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BLAppDelegate class]));
    }
}
