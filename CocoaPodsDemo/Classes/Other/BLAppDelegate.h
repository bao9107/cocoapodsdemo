//
//  BLAppDelegate.h
//  CocoaPodsDemo
//
//  Created by coco on 14-9-29.
//  Copyright (c) 2014年 zbl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
